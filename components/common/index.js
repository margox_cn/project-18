// 引入公用组件
import Loading from '~/components/common/Loading'
import ToastLoading from '~/components/common/ToastLoading'
import CountPicker from '~/components/common/CountPicker'
import Pagination from '~/components/common/Pagination'
import Dialog from '~/components/common/Dialog'

const CommonComponents = {
  install: function(Vue){
    Vue.component('loading', Loading)
    Vue.component('toast-loadiing', ToastLoading)
    Vue.component('count-picker', CountPicker)
    Vue.component('pagination', Pagination)
    Vue.component('v-dialog', Dialog)
  }
}

export default CommonComponents