import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    cartItems: [],
    userInfo: {}
  },
  getters: {
    userId (state) {
      return state.userInfo.id || null
    },
    userInfo (state) {
      return state.userInfo || {}
    },
    customerInfo (state) {
      return state.userInfo ? state.userInfo.customerInfo || {} : {}
    }
  },
  mutations: {
    setCartItems (state, cartItems) {
      state.cartItems = [ ...cartItems ]
    },
    addCartItem (state, cartItem) {
      state.cartItems = [ ...state.cartItems, cartItem ]
    },
    removeCartItem (state, cartItemKey) {
      state.cartItems = state.cartItems.filter(item => item.key !== cartItemKey)
    },
    setUserInfo (state, { override, userInfo }) {
      state.userInfo = override ? { ...userInfo } : { ...state.userInfo, ...userInfo }
    }
  }
})

export default store