const path = require('path')

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'netink',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /**
   * Router configuration
   */
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#696EE3' },
  /*
  ** Build configuration
  */
  serverMiddleware: [
    // API middleware
    '~/api/index.js'
  ],
  plugins: [
    {
      ssr: false, // only included on client-side
      src: '~plugins/initializer'
    }, {
      src: '~plugins/filters'
    }
  ],
  build: {
    vendor: ['axios'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        config.resolve.alias['scssinc'] = path.resolve(__dirname, './assets/scss/_inc.scss')
        config.resolve.alias['scssbase'] = path.resolve(__dirname, './assets/scss/_base.scss')
      }
    }
  }
}
