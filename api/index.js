const axios = require('axios')
const express = require('express')
const bodyParser = require('body-parser')
const wooCommerceConfig = require('../configs/woocommerce')

axios.defaults.baseURL = wooCommerceConfig.host
axios.defaults.params = {
  consumer_key: wooCommerceConfig.consumerKey,
  consumer_secret: wooCommerceConfig.consumerSecret,
}

const wpAPIBase = `/wp-json/wp/v2`
const wcAPIBase = `/wp-json/wc/v2`
const jwtAPIBase = `/wp-json/jwt-auth/v1`
const netinkAPIBase = `/wp-json/netink/v1`

const app = express()
const router = express.Router()

const formatResponse = (data) => ({ status: 200, data })

const formatError = (error, options = {}) => {

  options = {
    status: 500,
    message: '未知错误',
    statusMap: {},
    ...options
  }

  if (!error) {
    return {
      status: options.status,
      message: options.message
    }
  } else if (error.response) {
    return {
      status: error.response.status || options.status,
      message: options.statusMap[error.response.status] || (error.response.data || {}).message || options.message
    }
  } else {
    return {
      status: error.status || options.status,
      message: error.message || options.message
    }
  }

}

const getWPUser = async (request) => {

  const Authorization = request.header('Authorization')

  if (!Authorization) {
    return {
      status: 401,
      error: null,
      message: '用户未登录'
    }
  }

  try {

    const response = await axios.get(`${wpAPIBase}/users/me`, {
      headers: { Authorization }
    })

    if (response.data && response.data.id) {
      return response.data
    } else {
      return {
        status: 401,
        error: null,
        message: '用户未登录'
      }
    }

  } catch (error) {
    return formatError(error)
  }

}

// 获取用户信息
router.get('/customer', async (req, res, next) => {

  const response = await getWPUser(req)

  if (response.id) {

    try {

      const customerInfo = (await axios.get(`${wcAPIBase}/customers/${response.id}`)).data

      if (customerInfo && customerInfo.id) {
        res.send(formatResponse({ ...response, customerInfo }))
      } else {
        res.status(404).send({
          status: 404,
          message: '用户不存在'
        })
      }

    } catch (error) {
      error = formatError(error)
      res.status(error.status).send(error)
    }

  } else {
    res.status(response.status).send(response)
  }

})

// 更新用户信息
router.put('/customer', async (req, res, next) => {

  const response = await getWPUser(req)

  if (response.id) {

    try {

      const params = req.body
      const fieldsToBeUpdate = Object.keys(params)

      if (params.password && params.newPassword) {
        const checkResponse = await axios.post(`${jwtAPIBase}/token`, {
          username: response.username,
          password: params.password
        })
        if (checkResponse && checkResponse.data && checkResponse.data.token) {
          params.password = params.newPassword
          delete params.newPassword
        } else {
          res.status(500).send(formatError())
          return false
        }
      } else {
        delete params.newPassword
        delete params.password
      }

      if (params.woocommerceCustomerInfo) {
        await axios.put(`${wcAPIBase}/customers/${response.id}`, params.woocommerceCustomerInfo)
      }

      if (fieldsToBeUpdate.length !== 1 || fieldsToBeUpdate[0] !== 'woocommerceCustomerInfo') {
        delete params.woocommerceCustomerInfo
        await axios.post(`${wpAPIBase}/users/${response.id}`, params, {
          headers: {
            Authorization: req.header('Authorization')
          }
        })
      }

      res.send(formatResponse({
        id: response.id
      }))

    } catch (error) {
      error = formatError(error, {
        statusMap: {
          403: '原密码输入错误'
        }
      })
      res.status(error.status).send(error)
    }

  } else {
    res.status(response.status).send(response)
  } 

})

// 用户注册
router.post('/customer/signup', async (req, res, bext) => {

  try {

    const signupResponse = await axios.post(`${wpAPIBase}/users/register`, { ...req.body, role: 'customer'})

    if (signupResponse && signupResponse.data) {

      const signinResponse = await axios.post(`${jwtAPIBase}/token`, req.body)

      if (signinResponse && signinResponse.data && signinResponse.data.token) {
        res.send(formatResponse(signinResponse.data.token))
      } else {
        res.status(500).send(formatError())
      }

    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 用户登录
router.post('/customer/signin', async (req, res, bext) => {

  try {

    const signinResponse = await axios.post(`${jwtAPIBase}/token`, req.body)

    if (signinResponse && signinResponse.data && signinResponse.data.token) {
      res.send(formatResponse(signinResponse.data.token))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error, {
      statusMap: {
        403: '用户名或密码错误'
      }
    })
    res.status(error.status).send(error)
  }

})

// 获取购物车信息
router.get('/cart', async (req, res, next) => {

  try {

    const Cookie = req.header('Cookie')

    if (!Cookie) {
      res.send(formatResponse([]))
      return false
    }

    const response = await axios.get(`${wcAPIBase}/cart`, {
      params: { thumb: true },
      headers: { Cookie }
    })
  
    if (response && response.data && typeof response.data !== 'string') {
      res.send(formatResponse(Object.keys(response.data).map(key => {
        return {
          ...response.data[key],
          price: response.data[key].line_total / response.data[key].quantity
        }
      })))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 添加商品到购物车
router.post('/cart', async (req, res, next) => {

  try {

    const Cookie = req.header('Cookie')
    const response = await axios.post(`${wcAPIBase}/cart/add`, req.body, {
      headers: { Cookie }
    })

    if (response && response.data && typeof response.data !== 'string') {
      res.send(formatResponse(Object.keys(response.data).map(key => {
        return {
          ...response.data[key],
          price: response.data[key].line_total / response.data[key].quantity
        }
      })))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 从购物车移除商品
router.delete('/cart/:id', async (req, res, next) => {

  if (!req.params.id) {
    res.staus(404).send({
      status: 404,
      message: '请指定需要删除的购物车商品'
    })
    return false
  }

  const cart_item_key = req.params.id
  const Cookie = req.header('Cookie')

  try {

    const response = await axios.delete(`${wcAPIBase}/cart/cart-item?cart_item_key=${cart_item_key}`, {}, {
      headers: { Cookie }
    })

    if (response && response.data) {
      res.send(formatResponse(cart_item_key))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 获取产品列表
router.get('/products', async (req, res, next) => {

  try {

    const response = await axios.get(`${wcAPIBase}/products/`, { params: req.query })
    const products = response.data

    if (products && products instanceof Array) {
      res.send(formatResponse({
        total: parseInt(response.headers['x-wp-total'] || products.length, 10),
        items: products
      }))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 获取产品详情
router.get('/products/:id', async (req, res, next) => {

  if (!req.params.id) {
    res.staus(404).send({
      status: 404,
      error: null,
      message: '请指定商品ID'
    })
    return false
  }

  try {

    const product = (await axios.get(`${wcAPIBase}/products/${req.params.id}`)).data

    if (product && product.id) {
      res.send(formatResponse(product))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 获取产品可变属性列表
router.get('/products/:id/variations', async (req, res, next) => {

  try {

    const productVariations = (await axios.get(`${wcAPIBase}/products/${req.params.id}/variations`)).data

    if (productVariations && productVariations instanceof Array) {
      res.send(formatResponse(productVariations))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 获取产品可变属性
router.get('/products/:id/variations/:variation_id', async (req, res, next) => {

  try {

    const productVariation = (await axios.get(`${wcAPIBase}/products/${req.params.id}/variations/${req.params.variation_id}`)).data

    if (productVariation && productVariation.id) {
      res.send(formatResponse(productVariation))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

// 获取订单列表
router.get('/orders', async (req, res, next) => {

  const response = await getWPUser(req)

  if (response.id) {

    try {

      const params = { ...req.query, customer: response.id, thumb: true }
      const ordersResponse = await axios.get(`${wcAPIBase}/orders`, { params })
      const orders = ordersResponse.data

      if (orders && orders instanceof Array) {
        res.send(formatResponse({
          total: parseInt(ordersResponse.headers['x-wp-total'] || orders.length, 10),
          items: orders
        }))
      } else {
        res.status(500).send(formatError())
      }

    } catch (error) {
      error = formatError(error)
      res.status(error.status).send(error)
    }
  
  } else {
    res.status(response.status).send(response)
  }

})

router.get('/orders/counts', async (req, res, next) => {

  try {

    const orderCounts = (await axios.get(`${netinkAPIBase}/orders/count`, {
      headers: {
        Authorization: req.header('Authorization')
      }
    })).data

    if (orderCounts && orderCounts.total >= 0) {
      res.send(formatResponse(orderCounts))
    } else {
      res.status(500).send(formatError())
    }

  } catch (error) {
    error = formatError(error)
    res.status(error.status).send(error)
  }

})

netinkAPIBase

// 获取订单详情
router.get('/orders/:id', async (req, res, next) => {

  const response = await getWPUser(req)

  if (response.id) {

    try {

      const orderResponse = await axios.get(`${wcAPIBase}/orders/${req.params.id}`)
      const order = orderResponse.data

      if (order && order.id) {
        if (order.customer_id * 1 === response.id * 1) {
          res.send(formatResponse(order))
        } else {
          res.status(403).send({
            status: 403,
            error: null,
            message: '无权查看此订单'
          })
        }
      } else {
        res.status(500).send(formatError())
      }

    } catch (error) {
      error = formatError(error)
      res.status(error.status).send(error)
    }
  
  } else {
    res.status(response.status).send(response)
  }
  
})

// 创建订单
router.post('/orders', async (req, res, next) => {

  const response = await getWPUser(req)

  if (response.id) {
  
    try {

      const params = req.body

      if (!wooCommerceConfig.paymentMethods[params.payment_method]) {
        res.status(500).send(formatError({message: '结算信息有误'}))
        return false
      }

      if (!params.line_items || !params.line_items.length) {
        res.status(500).send(formatError({message: '结算信息有误'}))
        return false
      }

      params.payment_method_title = wooCommerceConfig.paymentMethods[params.payment_method]
      params.set_paid = params.payment_method === 'cod'
      params.billing = {
        ...params.billing,
        email: params.billing.email || 'guest@netink.margox.cn',
        city: '-',
        state: '-',
        country: 'CN',
      }
      params.shipping = params.billing

      const orderResponse = await axios.post(`${wcAPIBase}/orders`, { ...params, customer_id: response.id })
      const order = orderResponse.data

      if (order && order.id) {
        res.send(formatResponse(order))
      } else {
        res.status(500).send(formatError())
      }

    } catch (error) {
      error = formatError(error)
      res.status(error.status).send(error)
    }
  
  } else {
    res.status(response.status).send(response)
  }

})

app.use(bodyParser.json())
app.use(router)

module.exports = {
  path: '/api',
  handler: app
}
