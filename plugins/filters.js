import Vue from 'vue'

Vue.filter('formatTimeStamp', function(value) {
  if (!value) {
    return ''
  }
  const date = new Date()
  date.setTime(value)
  return date.toLocaleString()
})