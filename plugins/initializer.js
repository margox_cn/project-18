import Vue from 'vue'
import axios from 'axios'
import VModal from 'vue-js-modal'
import Notifications from 'vue-notification'
import AuthHelper from '~/helpers/Auth'
import CartHelper from '~/helpers/Cart'
import CustomerHelper from '~/helpers/Customer'
import OrderHelper from '~/helpers/Order'
import RouteHelper from '~/helpers/Route'
import CommonComponents from '~/components/common'
import { getCookie } from 'tiny-cookie'

window.eventBus = new Vue()

Vue.use(CommonComponents)
Vue.use(Notifications)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })

const initializeHelpers = async (context) => {

  AuthHelper.bindStore(context.app.store)
  CartHelper.bindStore(context.app.store)
  CustomerHelper.bindStore(context.app.store)
  OrderHelper.bindStore(context.app.store)

  RouteHelper.bindRouter(context.app.router)

}

const initializeAPIToken = () => {

  const token = getCookie('token')

  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  }

}

export default (context, inject) => {

  initializeAPIToken()
  initializeHelpers(context)

}