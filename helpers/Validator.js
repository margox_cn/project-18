export default {

  regExps: {
    number: /^\d*$/,
    phone: /^1\d{10}$/,
    email: /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/,
    nickname: /^\S{4,20}$/,
    username: /^[a-zA-Z0-9_-]{4,16}$/,
    password: /^\S{5,17}$/,
    url: /^[a-zA-z]+:\/\/(\w+(-\w+)*)(\.(\w+(-\w+)*))*(\?\S*)?$/,
    ip: /(d+).(d+).(d+).(d+)/,
    address: /^\S{10,400}$/
  },

  validate (value, type) {
    return this.regExps[type] ? this.regExps[type].test(value) : false
  },

  validateGroup (values, rules) {

    let invalids = {}
    let passed = true

    Object.keys(values).forEach(key => {

      if (!rules || !rules[key]) {
        return false
      }

      if (rules[key].requiredIf) {
        rules[key].required = !!values[rules[key].requiredIf]
      }

      if (rules[key].required === false && !values[key]) {
        return false
      }

      console.log(rules[key].equal, values[key], values[rules[key].equal])

      if (rules[key].equal && values[key] !== values[rules[key].equal]) {
        passed = false
        invalids[key] = {
          value: values[key],
          message: rules[key].message
        }
      } else if (rules[key].required !== false && !values[key]) {
        passed = false
        invalids[key] = {
          value: values[key],
          message: rules[key].message
        }
      } else if (rules[key].type && !this.validate(values[key], rules[key].type.toLowerCase())) {
        passed = false
        invalids[key] = {
          value: values[key],
          message: rules[key].message
        }
      } else if (rules[key].pattern && !new RegExp(rules[key].pattern).test(values[key])) {
        passed = false
        invalids[key] = {
          value: values[key],
          message: rules[key].message
        }
      }

    })

    return { invalids, passed }

  },

}