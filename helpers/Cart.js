import axios from 'axios'
import BaseConfig from '~/configs/base'
import AuthHelper from '~/helpers/Auth'

export default {

  store: null,

  bindStore (store) {
    this.store = store
  },

  async getCartItems () {
    try {
      const cartItems = (await axios.get(`${BaseConfig.apiURL}/cart`)).data.data
      this.store.commit('setCartItems', cartItems)
    } catch (error) {
      console.warn(error)
    }
  },

  async addCartItem (item) {
    try {
      await AuthHelper.checkAuth()
      await axios.post(`${BaseConfig.apiURL}/cart`, item)
      await this.getCartItems()
    } catch (error) {
      console.warn(error)
    }
  },

  async removeCartItem (id) {
    try {
      await axios.delete(`${BaseConfig.apiURL}/cart/${id}`)
      this.store.commit('removeCartItem', id)
    } catch (error) {
      console.warn(error)
    }
  }

}