import axios from 'axios'
import { setCookie, removeCookie } from 'tiny-cookie'
import { notify } from '~/utils/ui'
import { formatError } from '~/utils/base' 
import BaseConfig from '~/configs/base'
import CustomerHelper from '~/helpers/Customer'
import CartHelper from '~/helpers/Cart'

export default {

  store: null,

  bindStore (store) {
    this.store = store
  },

  async requestAuth () {
    return new Promise((success, failure) => {
      eventBus.$emit('request-authorize', { success, failure })
    })
  },

  async checkAuth (forceSync = false) {

    if (forceSync) {

    } else if (!this.store.state.userInfo.id) {
      return await this.requestAuth()
    } else {
      return true
    }

  },

  async signup (data) {

    try {

      const token = (await axios.post(`${BaseConfig.apiURL}/customer/signup`, data)).data.data

      setCookie('token', token)
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
      const { name } = await CustomerHelper.getCustomerInfo()
      notify(`你好，${name}!`, 'success')
      return true

    } catch (error) {
      error = formatError(error)
      notify(error.message, 'error')
      throw error
    }

  },

  async signin (data) {

    try {

      const token = (await axios.post(`${BaseConfig.apiURL}/customer/signin`, data)).data.data

      setCookie('token', token)
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
      CartHelper.getCartItems()
      const { name } = await CustomerHelper.getCustomerInfo()
      notify(`欢迎回来，${name}!`, 'success')
      return true

    } catch (error) {
      error = formatError(error)
      notify(error.message, 'error')
      throw error
    }

  },

  signout () {

    removeCookie('token')
    axios.defaults.headers.common['Authorization'] = ''
    this.store.commit('setUserInfo', { override: true, userInfo: {}})
    this.store.commit('setCartItems', [])

  },

}