import axios from 'axios'
import { notify } from '~/utils/ui'
import { formatError } from '~/utils/base' 
import BaseConfig from '~/configs/base'
import CartHelper from '~/helpers/Cart'
import AuthHelper from '~/helpers/Auth'
import ProductHelper from '~/helpers/Product'

export default {

  store: null,

  bindStore (store) {
    this.store = store
  },

  async getOrders (params) {

    try {
      return (await axios.get(`${BaseConfig.apiURL}/orders`, { params })).data.data
    } catch (error) {
      return []
    }

  },

  async getOrder (orderId) {

    try {
      return (await axios.get(`${BaseConfig.apiURL}/orders/${orderId}`)).data.data
    } catch (error) {
      return {}
    }

  },

  async getOrderCounts () {

    try {
      return (await axios.get(`${BaseConfig.apiURL}/orders/counts`)).data.data
    } catch (error) {
      return {
        total: 0,
        pending: 0
      }
    } 

  },

  async createOrder (order) {
  
    try {
      await AuthHelper.checkAuth()
      return (await axios.post(`${BaseConfig.apiURL}/orders`, order)).data.data
    } catch (error) {
      error = formatError(error)
      notify(error.message, 'error')
      throw error
    }

  },

  async updateOrder (cart_item_key) {

    try {

    } catch (error) {

    }

  },

  async removeOrder (order_id) {


  },

  async prepareCheckoutProducts ({ type, product_id, variation_id, quantity }) {

    if (!type) {
      return []
    }

    if (type === 'cart') {
      // 更新一次购物车列表
      await CartHelper.getCartItems()
      // 获取并返回购物车商品
      return JSON.parse(JSON.stringify(this.store.state.cartItems))
    } else if (type === 'single_product') {

      if (!product_id) {
        return []
      }

      product_id = product_id * 1
      variation_id = variation_id * 1
      quantity = isNaN(quantity) ? 1 : quantity * 1

      try {

        const productInfo = await ProductHelper.getProductInfo(product_id)

        if (variation_id !== 0) {
          const productVariationInfo = await ProductHelper.getProductVariation(product_id, variation_id)
          return [
            {
              ...productInfo,
              ...productVariationInfo,
              product_name: `${productInfo.name} - ${productVariationInfo.attributes.map(({ option }) => option).join('-')}`,
              product_id, variation_id, quantity,
              line_total: productInfo.price * quantity
            }
          ]
        } else {
          return [
            {
              ...productInfo,
              product_id, quantity,
              line_total: productInfo.price * quantity
            }
          ]
        }

      } catch (error) {
        console.warn(error)
        return []
      }

    }

  }

}