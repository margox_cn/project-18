export default {

  router: null,

  bindRouter (router) {
    this.router = router
  },

  navigateTo () {
    this.router.push(arguments)
  },

  redirectTo () {
    this.router.replace(arguments)
  }

}