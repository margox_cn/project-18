import axios from 'axios'
import { notify } from '~/utils/ui'
import { formatError } from '~/utils/base' 
import BaseConfig from '~/configs/base'

export default {

  store: null,

  bindStore (store) {
    this.store = store
  },

  async getCustomerInfo () {
    const userInfo = (await axios.get(`${BaseConfig.apiURL}/customer`)).data.data
    this.store.commit('setUserInfo', { userInfo })
    return userInfo
  },

  async updateCustomerInfo (data) {
    try {
      await axios.put(`${BaseConfig.apiURL}/customer`, data)
      await this.getCustomerInfo()
      notify(`信息已更新!`, 'success')
    } catch (error) {
      error = formatError(error)
      notify(error.message, 'error')
      throw error
    }
  }

}