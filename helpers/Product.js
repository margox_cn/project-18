import axios from 'axios'
import BaseConfig from '~/configs/base'
import AuthHelper from '~/helpers/Auth'
import RouteHelper from '~/helpers/Route'

export default {
  async buyProduct ({ productId, quantity, variationId }) {
    try {
      await AuthHelper.checkAuth()
      RouteHelper.router.push(`/checkout?type=single_product&product_id=${productId}&quantity=${quantity}&variation_id=${variationId}`)
    } catch (error) {
      console.log(error)
    }
  },
  async getProductInfo (productId) {
    try {
      return (await axios.get(`${BaseConfig.apiURL}/products/${productId}`)).data.data
    } catch (error) {
      return {}
      console.warn(error)
    }
  },
  async getProductVariations (productId) {
    try {
      return (await axios.get(`${BaseConfig.apiURL}/products/${productId}/variations`)).data.data
    } catch (error) {
      return []
      console.warn(error)
    }
  },
  async getProductVariation (productId, variationId) {
    try {
      return (await axios.get(`${BaseConfig.apiURL}/products/${productId}/variations/${variationId}`)).data.data
    } catch (error) {
      return {}
      console.warn(error)
    }
  },
  async getProducts (params = {}) {
    try {
      // WooCommerce的bug，include参数的第一个id对应的商品不会被返回
      params.include && (params.include = [1, ...params.include])
      return (await axios.get(`${BaseConfig.apiURL}/products`, { params })).data.data
    } catch (error) {
      return []
      console.warn(error)
    }
  }
}