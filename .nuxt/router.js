import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _62d436ae = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)
const _e6c6fa9e = () => import('../pages/index/index.vue' /* webpackChunkName: "pages/index/index" */).then(m => m.default || m)
const _f368570c = () => import('../pages/index/checkout/index.vue' /* webpackChunkName: "pages/index/checkout/index" */).then(m => m.default || m)
const _0bf06e68 = () => import('../pages/index/account.vue' /* webpackChunkName: "pages/index/account" */).then(m => m.default || m)
const _3255864f = () => import('../pages/index/account/index.vue' /* webpackChunkName: "pages/index/account/index" */).then(m => m.default || m)
const _7da97b36 = () => import('../pages/index/account/settings.vue' /* webpackChunkName: "pages/index/account/settings" */).then(m => m.default || m)
const _54a9103b = () => import('../pages/index/account/orders/index.vue' /* webpackChunkName: "pages/index/account/orders/index" */).then(m => m.default || m)
const _e053999e = () => import('../pages/index/account/address.vue' /* webpackChunkName: "pages/index/account/address" */).then(m => m.default || m)
const _36a0f455 = () => import('../pages/index/account/orders/page/_page.vue' /* webpackChunkName: "pages/index/account/orders/page/_page" */).then(m => m.default || m)
const _1f6c4df1 = () => import('../pages/index/account/orders/_orderId.vue' /* webpackChunkName: "pages/index/account/orders/_orderId" */).then(m => m.default || m)
const _4c5769d0 = () => import('../pages/index/products/index.vue' /* webpackChunkName: "pages/index/products/index" */).then(m => m.default || m)
const _2a87f677 = () => import('../pages/index/products/search/_keyword.vue' /* webpackChunkName: "pages/index/products/search/_keyword" */).then(m => m.default || m)
const _eaeaabd6 = () => import('../pages/index/products/_productId.vue' /* webpackChunkName: "pages/index/products/_productId" */).then(m => m.default || m)
const _4bbfad52 = () => import('../pages/index/checkout/_orderId.vue' /* webpackChunkName: "pages/index/checkout/_orderId" */).then(m => m.default || m)



const scrollBehavior = function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/",
			component: _62d436ae,
			children: [
				{
					path: "",
					component: _e6c6fa9e,
					name: "index"
				},
				{
					path: "checkout",
					component: _f368570c,
					name: "index-checkout"
				},
				{
					path: "account",
					component: _0bf06e68,
					children: [
						{
							path: "",
							component: _3255864f,
							name: "index-account"
						},
						{
							path: "settings",
							component: _7da97b36,
							name: "index-account-settings"
						},
						{
							path: "orders",
							component: _54a9103b,
							name: "index-account-orders"
						},
						{
							path: "address",
							component: _e053999e,
							name: "index-account-address"
						},
						{
							path: "orders/page/:page?",
							component: _36a0f455,
							name: "index-account-orders-page-page"
						},
						{
							path: "orders/:orderId?",
							component: _1f6c4df1,
							name: "index-account-orders-orderId"
						}
					]
				},
				{
					path: "products",
					component: _4c5769d0,
					name: "index-products"
				},
				{
					path: "products/search/:keyword?",
					component: _2a87f677,
					name: "index-products-search-keyword"
				},
				{
					path: "products/:productId?",
					component: _eaeaabd6,
					name: "index-products-productId"
				},
				{
					path: "checkout/:orderId?",
					component: _4bbfad52,
					name: "index-checkout-orderId"
				}
			]
		}
    ],
    
    
    fallback: false
  })
}
