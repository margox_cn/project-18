export const formatResponse = (data) => ({ status: 200, data })

export const formatError = (error, options = {}) => {

  options = {
    status: 500,
    message: '未知错误',
    statusMap: {},
    ...options
  }

  if (!error) {
    return {
      status: options.status,
      message: options.message
    }
  } else if (error.response) {
    return {
      status: error.response.status || options.status,
      message: options.statusMap[error.response.status] || error.response.data.message || options.message
    }
  } else {
    return {
      status: error.status || options.status,
      message: error.message || options.message
    }
  }

}