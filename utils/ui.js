export const toast = (params) => {
  params = typeof params === 'string' ? { text: params } : params
  eventBus && eventBus.$emit('request-toast', params)
}

export const hideToast = () => {
  eventBus && eventBus.$emit('request-hide-toast')
}

export const toastLoading = (params) => {
  params = typeof params === 'string' ? { text: params } : params
  eventBus && eventBus.$emit('request-toast-loading', params)
}

export const hideToastLoading = () => {
  eventBus && eventBus.$emit('request-hide-toast-loading')
}

export const notify = (params, type = 'info') => {
  params = typeof params === 'string' ? { title: params, type } : params
  eventBus && eventBus.$emit('request-notify', params)
}

export const alert = (params) => {
  params = typeof params === 'string' ? { title: params } : params
  eventBus && eventBus.$emit('request-alert', params)
}

export const confirm = (params) => {
  params = typeof params === 'string' ? { title: params } : params
  eventBus && eventBus.$emit('request-confirm', params)
}

export default {
  toast, toastLoading, notify, alert, confirm
}
