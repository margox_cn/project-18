export const orderStatusMap = {
  'pending': '待付款',
  'processing': '处理中',
  'hold': '保留',
  'completed': '已完成',
  'cancelled': '已取消',
  'refunded': '已退款',
  'failed': '失败',
  'shipping': '已发货',
}