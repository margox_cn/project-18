export const customerAddressRules = {
  address: {
    type: 'address',
    message: '请填写有效的收货地址',
  },
  name: {
    pattern: /^\S{1,20}$/,
    message: '请填写有效的收货人姓名',
  },
  phone: {
    type: 'phone',
    message: '请填写有效的手机号码',
  },
  postcode: {
    required: false,
    type: 'number',
    message: '请填写有效的邮政编码，或者留空'
  }
}

export const userSettingsRules = {
  name: {
    type: 'nickname',
    message: '请输入有效的用户昵称'
  },
  email: {
    type: 'email',
    message: '请输入有效的用户邮箱'
  },
  password: {
    type: 'password',
    message: '请输入原密码',
    requiredIf: 'newPassword'
  },
  newPassword: {
    required: false,
    type: 'password',
    message: '请输入新密码',
  },
  repeatNewPassword: {
    requiredIf: 'newPassword',
    equal: 'newPassword',
    message: '请确认新密码',
  },
}

export const checkoutRules = {
  address: {
    type: 'address',
    message: '请填写有效的收货地址',
  },
  name: {
    pattern: /^\S{1,20}$/,
    message: '请填写有效的收货人姓名',
  },
  phone: {
    type: 'phone',
    message: '请填写有效的手机号码',
  },
  postcode: {
    required: false,
    type: 'number',
    message: '请填写有效的邮政编码，或者留空'
  }
}