# netink

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

#### TODO
- 添加购物车时带上商品图片[100%]
- 完善注册和登录[100%]
- 完善结算流程[80%]
- 完成订单列表页面[90%]
- 完成账户信息页面[50%]
- 完成收货地址页面[100%]
- 完成设置页面[50%]
- 完善接口认证逻辑[90%]
- 完成支付功能[0%]
- 完善网站交互[80%]
- 公用组件和CSS代码的抽离与优化[80%]
